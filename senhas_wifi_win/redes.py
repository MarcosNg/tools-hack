#!python3
# -*- coding: utf-8 -*-

"""
recuperar senhas do windows - install python >= 3.5
"""

import subprocess as cmd

def executar_comando(comando):
    saida = cmd.getoutput(comando)
    return saida.split('\n')

def buscar_rede(linha, chave):
    if chave in linha:
        return linha.split(chave)[1]

def buscar_chave(linha, chave):
    if chave in linha:
        sep = linha.split(':')
        return sep[1]
    return "None"
    

# saida contem todos os nomes de wifi, vejo todos
saida = executar_comando("netsh wlan show profiles")

arquivo = open('senhas.txt','w')

for i in saida:

    lista = i.split(' ')

    if str(lista[0]) == 'Todos':
        
        tmp = buscar_rede(i, 'rios:') # remover todos o primeiro espaco - ok
        rede = ''
        for i in range(1,len(tmp)):
            rede += tmp[i]
            
        
        comando_rede = 'netsh wlan show profile name= "%s" key=clear' %(rede)
        informacoes_conexao = executar_comando(comando_rede)
        for j in informacoes_conexao:
            resultado = buscar_chave(j, "do da Chave            :")
            if resultado != "None":
                break
            
        print('chave da rede ',rede,' = ',resultado)
        arquivo.write('%s = %s\n' %(rede, resultado))
            
arquivo.close()
